# Bash

## Symbolic link

How to create a symbolic link:


```bash
ln -s /home/joanmarc/projects/bible/bash/.scripts.sh /home/joanmarc/.scripts.sh
```

## Zip and unzip

Zipped file extension is *.tar.gz*. You can deal with them with two main commands:

### Zip

How to zip a folder:
```bash
tar -czvf filename.tar.gz folder/to/zip/
```

Examples:
```bash
tar -czvf modules.tar.gz trytond/modules/
tar -czvf modules.tar.gz *
```

### Unzip

How to unzip a file:
```bash
tar -xzvf filename.tar.gz
```

Example:

```bash
cd trytond/modules/
tar -xzvf modules.tar.gz
```

## Netcat

Netcat is useful to transfer data in a net. There are two sides: server and client. If we want to transfer *testfile* we have to set the server host to open a port and point to a file.

```bash
nc -l 2389 > modules.tar.gz
```

So the server is waiting for the client to receive a file. In the client side, just put:

```bash
cat modules.tar.gz | nc 192.168.0.17 2389
```

## Utils

Kill process allocating a port

```bash
sudo kill -9 'sudo lsof -t -i:5432'
```
