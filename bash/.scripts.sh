#!/bin/bash

# Projects shortcuts

function telecos(){
  nemo ~/projects/telecos
}
function intellij(){
  bash ~/Escritorio/idea-IC-222.4345.14/bin/idea.sh
}
function split() {
  xdotool key ctrl+shift+e
  xdotool key ctrl+shift+o
  xdotool key ctrl+shift+p
  xdotool key ctrl+shift+p
  xdotool key ctrl+shift+o
  xdotool key ctrl+shift+p
}
function areagestio() {
  cd ~/projects/bages
}
function authentication_dummy() {
  cd ~/projects/authentication_dummy
}
function bages() {
  cd ~/projects/bages
}
function bible(){
  cd ~/projects/bible
}
function calfruitos() {
  cd ~/projects/calfruitos
}
function calfruitos6() {
  cd ~/projects/calfruitos6
}
function calidae() {
  cd ~/projects/calidae
}
function delegates() {
  cd ~/projects/delegates
}
function gestionactivos() {
  cd ~/projects/gestionactivos
}
function gigantedelcolchon() {
  cd ~/projects/gigantedelcolchon
}
function pratginestos() {
  cd ~/projects/pratginestos
}
function vidrala(){
  cd ~/projects/vidrala
}

function ops_aws(){
  export AWS_DEFAULT_REGION=eu-west-1
  aws --profile=calify ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 082441762723.dkr.ecr.eu-west-1.amazonaws.com
  bash configure.sh --dev
}

# Git commands

function gco(){
  git checkout
}
function gfpa(){
  git fetch --prune --all
}

function destroy_branches(){
  git checkout master
  git branch | grep -v "master" | xargs git branch -D 
}

function run_webapp(){
  workon fruitoswebapp
  export DJANGO_LOCAL=true
  export DJANGO_DISABLE_CSRF=true
  export DJANGO_SETTINGS_MODULE=calfruitos.settings.development
  python manage.py runserver
}

function update_tryton_server() {
  trytond-admin -c config.cfg --logconf log.cfg -d $1 -u $1
}

function get_db_extra_url(){
  export TRYTOND_DATABASE_URI=postgresql://joanmarc:joanmarc@/
  export PIP_EXTRA_INDEX_URL=https://pypi.calidae.net/public
}


function my_ip(){
  dig +short myip.opendns.com @resolver1.opendns.com
}

function vpncalidae(){
  cd /home/joanmarc/Escritorio/vpncalidae/
  sudo openvpn pfsense-TCP4-1194-joanmarc.ovpn
}

function vpnaranow(){
  workon aranow
  cd pfsense-udp-1194-calidae-dev/
  sudo openvpn pfsense-udp-1194-calidae-dev.ovpn
}

# Tryton

function tryton42(){
  workon tryton42
  tryton
}
function tryton46(){
  workon tryton46
  tryton
}

function tryton48(){
  workon tryton48
  tryton
}

function tryton52(){
  cd /home/joanmarc/projects/tryton/tryton52
  source bin/activate
  tryton
}

function tryton54(){
  workon tryton54
  tryton
}

function tryton58(){
  cd /home/joanmarc/projects/tryton/tryton58
  source bin/activate
  tryton
}

function tryton60(){
  cd /home/joanmarc/projects/tryton/tryton60
  source bin/activate
  tryton
}

function tryton62(){
  workon tryton62
  tryton
}

function tryton66(){
  workon tryton66
  tryton
}

# Docker scripts

function django(){
  docker-compose run --rm --service-ports django bash
}

function drun(){
  docker-compose run --rm
}

function druns(){
  docker-compose run --rm --service-ports
}

function tunnel_vidrala_bi(){
  echo Obert el port 5555 per les bases de dades bi
  ssh calidae.jenkins -L 5555:localhost:22222 ssh vidrala.tryton -L 22222:192.168.10.183:5432
}

function tunnel_vidrala_tryton(){
  echo Obert el port 8001 pel tryton
  ssh calidae.jenkins -L 8001:localhost:33333 ssh vidrala.tryton -L 33333:localhost:8000
}

function tunnel_vidrala_database(){
  echo Obert el port 5556 pel dbeaver
  ssh calidae.jenkins -L 5556:localhost:44444 ssh vidrala.tryton -L 44444:localhost:5432
}

removecontainers() {
    docker stop $(docker ps -aq)
    docker rm $(docker ps -aq)
}

armageddon() {
    removecontainers
    docker network prune -f
    docker rmi -f $(docker images --filter dangling=true -qa)
    docker volume rm $(docker volume ls --filter dangling=true -q)
    docker rmi -f $(docker images -qa)
}

# Devops
login-calimesdae() {
  export AWS_PROFILE=calimesdae
  export AWS_REGION=eu-west-1
  aws --profile=calimesdae ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 510746767927.dkr.ecr.eu-west-1.amazonaws.com
}

login-calify() {
  export AWS_PROFILE=calify
  export AWS_REGION=eu-west-1
  aws --profile=calify ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 082441762723.dkr.ecr.eu-west-1.amazonaws.com
}

login-calidae() {
  export AWS_PROFILE=calidae
  export AWS_REGION=eu-west-1
  aws --profile=calify ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 516696541228.dkr.ecr.eu-west-1.amazonaws.com
}

tunnel-cardona() {
  autossh -M 0 cardona.calidae.net -l joanmarc -L 0.0.0.0:5000:dev-bursting-imp.caire3dulodu.eu-west-1.rds.amazonaws.com:5432 -L 0.0.0.0:5001:terraform-20200612085105820300000001.caire3dulodu.eu-west-1.rds.amazonaws.com:5432 -N -v
}

tunnel-bursting() {
  autossh -M 0 cardona.calidae.net -l joanmarc -L 0.0.0.0:5000:dev-bursting-imp.caire3dulodu.eu-west-1.rds.amazonaws.com:5432 -N -v
}

tunnel-humana() {
  autossh -M 0 cardona.calidae.net -l joanmarc -L 0.0.0.0:5000:dev-bursting-imp.caire3dulodu.eu-west-1.rds.amazonaws.com:5432 -L 0.0.0.0:5002:humana-clasificacion-rds.caire3dulodu.eu-west-1.rds.amazonaws.com:5432 -N -v
}