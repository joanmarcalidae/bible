# POSTGRESQL


## Introducció

SQL (Structured Query Language o Llenguatge d'interrogació estructurat) és un llenguatge estàndard de comunicació amb bases de dades relacionals. És a dir, un llenguatge normalitzat que permet treballar amb la majoria de bases de dades relacionals. L'SQL es pot hostatjar (es pot utilitzar) dins d'altres llenguatges de programació. La principal característica d'aquest llenguatge és la seua simplicitat, ja que amb pocs coneixements es poden fer consultes bàsiques sobre una base de dades, encara que no per això deixa de ser un llenguatge complet, tant relacionalment com computacionalment (a partir de la versió SQL3 publicada el 1999). 


## Característiques 

### Funcionalitat

SQL no només es limita a la conulta de dades, sinó que exerceix el paper de llenguatge de definició de dades (DDL), llenguatge de definició de vistes (LDV) i llenguatge de manipulació de dades (LMD).

### Modes d'ús

Permet dos modes d'ús:
1. **Interactiu**: destinat principalment als usuaris finals avançats o ocasionals, en el que les diverses sentències SQL s'escriuen i s'executen a la línia d'ordres, o un entorn semblant.

2. **Integrat**: destinat a l'ús per part dels programadors dins de programes escrits en qualsevol llenguatge de programació amfitrió. En aquest cas SQL agafa el paper de subllenguatge de dades.

### Optmització

SQL és un llenguathe declaratiu, és a dir s'especifica el que es necessita i no com aconseguir-ho. L'ordre d'execució requereix una optimització per a no afectar l'eficiència del SGBD, i això està resolt amb la construcció d'índex.


## Comandes

Per poder explicar les comandes, crearem una base de dades d'exemple.

### CREATE
```bash
joanmarc@calidae:~/$ psql
joanmarc=# CREATE TABLE Persones (ID int, Nom VARCHAR(100), Cognom VARCHAR(100), Pais VARCHAR(100), Naixement DATE);
```

### INSERT

Popularem la base de dades amb 4 registres:

```bash
joanmarc=# INSERT INTO Persones (Nom, Cognom, Pais, Naixement) VALUES ('Joan Marc', 'Soler', 'Catalunya', '1980-02-01'), ('Benito', 'Camela', 'Espanya', '1988-08-08'), ('Aitor', 'Tilla', 'Euskadi', '2000-01-01'), ('Aitor', 'Menta', 'Catalunya', '2020-01-01');
```

### SELECT

El resultat de la creació de la base de dades i l'insert es podrà veure mitjançant la comanda SELECT, que retorna els registres d'una determinada taula. En el nostre exemple:

```bash
joanmarc=# SELECT * FROM Persones;
```

```bash
 id |    nom    | cognom |   pais    | naixement
----+-----------+--------+-----------+------------
    | Joan Marc | Soler  | Catalunya | 1980-02-01
    | Benito    | Camela | Espanya   | 1988-08-08
    | Aitor     | Tilla  | Euskadi   | 2000-01-01
    | Aitor     | Menta  | Catalunya | 2020-01-01
```



### Select

Retorna les columnes especificades d'una taula d'uns registres que compleixen una condició. Per exemple:

```sql
SELECT Nom, Cognom
FROM Persones
WHERE Pais='Catalunya';
```
```bash
    nom    | cognom
-----------+--------
 Joan Marc | Soler
 Aitor     | Menta
```

### Distinct

Elimina els valors repetits dins de la consulta. Per exemple, si volem una llista dels països de la taula anterior, però no volem que ens retorni els repetits:

Command:

```sql
SELECT DISTINCT Pais
FROM Persones;
```
```bash
   pais
-----------
 Catalunya
 Espanya
 Euskadi
```

### Insert

Afegeix registres a una taula determinada especificant els valors de cadascuna de les columnes. Per exemple, si volem afegir una nova persona a la nostra base de dades:

Command:

```sql
INSERT INTO Persones (Nom, Cognom, Pais)
VALUES ('Leo', 'Messi', 'Portugal'), ('Zlatan', 'Ibrahimovic', 'Suècia');
```
```bash
 id |    nom    |   cognom    |   pais    | naixement
----+-----------+-------------+-----------+------------
    | Joan Marc | Soler       | Catalunya | 1980-02-01
    | Benito    | Camela      | Espanya   | 1988-08-08
    | Aitor     | Tilla       | Euskadi   | 2000-01-01
    | Aitor     | Menta       | Catalunya | 2020-01-01
    | Leo       | Messi       | Portugal  |
    | Zlatan    | Ibrahimovic | Suècia    |
```

### Update

En l'exemple anterior hem afegit dos registres de persones però sense data de naixement (ja que aquesta columna no era requerida). En el cas que ara sapiguem la data d'un d'ells i la volguem introduïr i a més volguem canviar el Pais perquè ens haviem equivocat.

Command:

```sql
UPDATE Persones
SET Pais = 'Argentina', Naixement = '1987-06-30'
WHERE Nom = 'Leo' AND Cognom = 'Messi';
```

```bash
 id |    nom    |   cognom    |   pais    | naixement
----+-----------+-------------+-----------+------------
    | Joan Marc | Soler       | Catalunya | 1980-02-01
    | Benito    | Camela      | Espanya   | 1988-08-08
    | Aitor     | Tilla       | Euskadi   | 2000-01-01
    | Aitor     | Menta       | Catalunya | 2020-01-01
    | Leo       | Messi       | Argentina | 1987-06-30
    | Zlatan    | Ibrahimovic | Suècia    |
```

# Delete

Finalment, podem eliminar els registres. Per exemple, si volem eliminar la persona Zlatan Ibrahimovic:

```sql
DELETE FROM Persones
WHERE Nom = 'Zlatan' and Cognom = 'Ibrahimovic';
```
```bash
 id |    nom    | cognom |   pais    | naixement
----+-----------+--------+-----------+------------
    | Joan Marc | Soler  | Catalunya | 1980-02-01
    | Benito    | Camela | Espanya   | 1988-08-08
    | Aitor     | Tilla  | Euskadi   | 2000-01-01
    | Aitor     | Menta  | Catalunya | 2020-01-01
    | Leo       | Messi  | Argentina | 1987-06-30
```