# WSGI

The Web Server Gateway Interface (WSGI) is a simple calling convention for web servers to forward requests to web applications or frameworks written in the Python programming language.

## Example aplication

Let's going to explain a setting up Django and a web server using nginx and WSGI.

### Concept

The route of the communication is:

```
the web client <-> the web server <-> the socket <-> uwsgi <-> Django
```

The user makes a petition with web client, so he sends a petition through the port 80 to a server called www.django-example.com.

The nginx is the web server that is listening for some ports, included the port 80. There is a configuration file *mysite_nginx.conf* hosted in *etc/nginx/*

More info:
https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html

## WSGI Emperor

If you have a large number of apps to deploy there is uWSGI instance called uWSGI Emperor that allow you to multi-deploy.

By default, the Emperor will scan specific directores for supported uWSGI configuration files (.ini, .yml, .json, etc.).
https://uwsgi-docs.readthedocs.io/en/latest/Emperor.html