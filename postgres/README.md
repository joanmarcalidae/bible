# POSTGRESQL

PostgreSQL is an open source database manager developed by PGDG community. In
software development usually are two kinf of environments: development and
production.


## Installing

You can install postgresql with the following command:
`$ sudo apt-get install postgresql`


## First access to PSQL

You can run `psql` in your bash to access to the postgres environment, where
you can  manage databases. You will access to psql with your current user. So
if it is the first time, probably this user does not exist in psql, and you
will get this error:

```bash
psql: FATAL:  role "username" does not exist
```

So you have to access with *postgres* user:

```bash
$ sudo -u postgres psql
```

Now you are in postgres environment that looks like this:

```postgres
psql (10.6 (Ubuntu 10.6-0ubuntu0.18.04.1))
Type "help" for help.

postgres=#
```

Here you can list users and you will notice that only *postgres* user exists.
For listing, run the command `\du`:

```bash
                              List of roles
 Role name |                      Attributes                      | Member of
-----------+------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
```

Now you can create a user that matches with your linux user:

```postgres
postgres=# create user joanmarc;
CREATE ROLE
```

Then, you must give it a password:

```postgres
postgres=# alter user joanmarc with password 'joanmarc';
ALTER ROLE
```

Finally, you have to give it all privileges:

```postgres
alter role joanmarc with superuser createdb createrole replication bypassrls;
```

If you list again users you get the following:

```postgres
                              List of roles
 Role name |                      Attributes                      | Member of
-----------+------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 joanmarc  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
```

So finally if you get out of postgres environment with `\q` command and come back to the bash.

Here you must create a database with your username:

```bash
$ createdb joanmarc
```

Finally, you can run `psql` and you will able to access to psql environment.


## Commands

```bash
`\l` list of databases
`\du` list of roles with atributtes
```

## Dump and restore databases

If you have a database somewhere, you can dump it (put all data in a file) and
restore it in another place (put this data in other database).

In order to dump a database you run the following command:

```bash
$ pg_dump -h somserver.com -p 5432 -d database -U user -Fc > filename.dump
```

So for example, if we have a database in google.com (which allow access with port 5432), and this database is named *dbgoogle* and owned by *usrgoogle*, you must write:

```bash
$ pg_dump -h google.com -p 5432 -d dbgoogle -U usrgoogle -Fc > google.dump
```

Once we have the *google.dump* file in our current directory, we can restore it. So we must create a database to restore it:

```bash
$ createdb google
```

A database named *google* is created by your current user *joanmarc*. So now we
can restore it:

```bash
$ pg_restore --no-owner -U joanmarc -d google --role=joanmarc google.dump
```

Notice that *--no-owner* flag is needed because the original database will
have different owners.

By default `-h localhost -p 5432`, so it is not needed to be writen.
