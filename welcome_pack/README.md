# Init Pack


# Primer dia

0. Assignar un tutor. Aquest tutor ha de ser una referència pel nouvingut però en cap cas l'única persona que ha d'ensenyar-li. El que anomenem *caudillo*. Això es fa a la sala Ramallets el primer dia a primera hora i s'aprofita per fer una introducció inicial a Calidae, explicant com es treballa a Calidae i com s'organitzarà la seva estancia.

1. Entregar el portàtil formatejat.

2. Llegir el correu de benvinguda enviat pel Jaume.

3. Llegir el welcome-pack i assignar-li. Fer incís en que aquest s'ha de llegir i mantenir.

4. Ensenyar les instal·lacions i presentar als equips.

5. Mirar documentació:
    - Fer el tutorial de djangogirls: https://tutorial.djangogirls.org/es/
    - Fer el tutorial de git: https://learngitbranching.js.org/
    - Fer el turorial de python: https://www.learnpython.org/es/

6. Crear compte:
    - Bitbucket
    - Syspass
    - Jenkins

7. Introduïr als authorized_keys del jenkins la seva clau ssh per a que serveixi de pont als servidors.


8. Passar llista de programes amb els que s'ha de començar a familiaritzar:
    - Ansible
    - Terraform
    - DBeaver
    - Docker
    - Git
    - Tryton
    - Vagrant
    - Visual Studio Code

9. Passar-li l'enllaç del repositori Bible. Actualment es poden entendre bastant bé els apartats: ansible, docker, postgres, vagrant i virtualenvwrapper.
