#!/usr/bin/env python

import gnupg

gpg = gnupg.GPG(use_agent=True)

with open('vault.gpg', 'rb') as cryptogram:
    datagram = gpg.decrypt_file(cryptogram)
    print(datagram.data.decode().strip())
