#  ANSIBLE

Ansible is a tool that allow to configure and manage machines.

It manages nodes through SSH connections and doesn't need any addicional
software but the python in order to install it.

It has modules that works with JSON and it standard output is YAML.

The inventory is a description of the nodes that Ansible can access to.
This inventory is defined with a configuration file, with INI format, and
its ubication usually is /etc/ansible/hosts. In this configuration file
we usually hav IP directions that ansible can use. For example:

```yaml
192.168.6.1

[webservers]
foo.example.com
bar.example.com
```

Once nodes are defined we can talk about playbooks. Playbooks describe
configuration, deployment and orquestration in Ansible.
Each playbook jouns a set of hosts with a set of roles. Each role is
represented by calls to ansible that Ansible defines as tasks.

So basically a task is a call to a module of Ansible.

A playbook can have some plays. Let's see a simple-play example:

```yaml
---
- hosts: webservers
  vars:
    http_port: 80
    max_clients: 200
  remote_user: root
  tasks:
  - name: Asegurarse que Apache este en la última versión
    yum: pkg=httpd state=latest
  - name: Escribir el archivo de configuración de apache
    template: src=/srv/httpd.j2 dest=/etc/httpd.conf
    notify:
    - Reiniciar Apache
  - name: Asegurarse que Apache esta ejecutando (y habilitarlo al iniciar el sistema)
    service: name=httpd state=started enabled=yes
  handlers:
    - name: Reiniciar Apache
      service: name=httpd state=restarted
```

Each tasks is executed (one by one) against every machine that matches with
the host pattern.

A task is composed by:
- name: Human readable output useful to provide good descriptions
- module: options

We have a list of modules here:
https://docs.ansible.com/ansible/latest/modules/list_of_all_modules.html

So, for example:

```yaml
apt:
  name: pip
  update_cache: yes
```

Does:

```bash
$ apt-get update pip
$ apt install pip
```

## Example: Installing a Django for Immo

Example with jenkins: We want to deploy a Django for Immo.
In jenkins machine we have the configure job, that have:
Playbook path: /opt/calidae/ansible/configure.yml
Host subset: calidae.dev.immo

Host subset is the --limit parameter. This parameter could be:

```bash
"host1"
"host1,host2"
(With double quotes)

'group1'
(With simple quotes)
```

So it's the same as do:

```bash
ansible-playbook /opt/calidae/ansible/configure.yml --limit "calidae.dev.immo"
```

It plays the configure.yml limiting to the calidae.dev.immo host. The configure
playbook contains the following:

```yaml
- hosts: django
  roles:
    - system_user
    - rsyslog
    - pg_server
    - pg_config
    - django
    - django_update
    - uwsgi
    - letsencrypt
    - nginx
```

The other steps with hosts that does not match with calidae.dev.immo are
ignored. Django hosts matches because in hosts.ini we have:

```yaml
[django]
alembeeks.delegates
alembeeks.immo
calidae.dev.delegates
calidae.dev.dentalresidency
calidae.dev.fcb_dashboard
calidae.dev.gestionactivos
calidae.dev.immo
calidae.dev.memoriaesquerra
calidae.dev.santasusanna_tancaments
dentalresidency.plataforma
memoriaesquerra.web
```

So ansible is going to play 'django' roles one by one. The first one is
system_user, so we go to the folder /roles/system_user and we have:

```bash
-- system_user
 |- tasks
  |- main.yml
```

The main.yml contains the following:

```yaml
- name: create system user
  user:
    name: '{{ system_user.name }}'
    shell: /bin/bash
    createhome: True
    home: '{{ system_user.home }}'
```

So that will execute 'create system user'. The template used the group vars.
In this cais, it will use 'django.yml' (because we used hosts: django). If
we look at this file:

```yaml
type: django
python_version: 3.6
system_user:
  home: /opt/django
  name: django
```

So '{{ system_user.name }}' = django
   '{{ system_user.home }}' = /opt/django
