# TERRAFORM

Terraform is a framework developed by Hashicorp that allow to manage deployments with code.

## Resources in AWS

Resources define the data types and API interactions required to create, update, and destroy infrastructure with a cloud vendor while the Terraform state stores mapping and metadata information for those remote objects.

The most common provider of resources is Amazon Web Services (AWS) that has a lot of kind of resources. The most common are:

EC2: Elastic Cloud Computing
RDS: Related Database Service
S3: Simple Storage Server

In order to understand this resources, we can redefine it:

EC2: A server that has an operative system and can be used as a computer
RDS: A database that makes it querys quick
S3: A cloud where you can store a huge ammount of data

In order to create an EC2 instance we must use an Amazon Machine Image (AMI).


## Example: Deploying a EC2 instance

Let's going to see an example of deployment to an EC2 instance of AWS. First of all, we must define a provider in a file *main.tf*:

```terraform
provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}
```

This variables are stored in another file *variables.tf*:

```terraform
variable "access_key" {
    TF_VAR_access_key=$_KEY
    AWS_ACCESS_KEY_ID=$_KEY
}
variable "secret_key" {
    TF_VAR_secret_key=$_SECRET
    AWS_SECRET_ACCESS_KEY=$_SECRET
}

variable "region" {
  default = "eu-west-1"
}

variable "my_ami" {
  default = "ami-132a45b67d8e9"
}
```

In this case, variables are preceded with $ because are get from environment.

Finally, we specify the resource:

```terraform
resource "aws_instance" "ec2_fruitos48_dev" {
  ami           = "${var.my_ami}"
  instance_type = "t3.small"
  key_name      = "my_ami"

  tags {
    Name = "fruitos48.dev"
  }

  provisioner "local-exec" {
        command = "sleep 120; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu --private-key ./deployer.pem -i '${aws_instance.jenkins_master.public_ip},' master.yml"
}
}
```

So *ami* is the one specified in variables, with his type and key name.

