# VAGRANT

Vagrant is a program that allows to create virtual boxes to simulate servers in
your computer. There is a list of possible machines here:
[https://app.vagrantup.com/boxes/search]


## Prerrequisites

It is necessary to install vagrant and virtualbox:

```bash
$ sudo apt-get install vagrant
$ sudo apt-get install virtualbox
```

## Initialize a machine

In order to initialize a Vagrant machine, you can run this command:

```bash
$ vagrant init ubuntu/bionic64
```

## Configuration file

Once you initialized the machine, a Vagrantfile configuration file is created.
You can modify this file to change name of virtual box, ports, etc.

Here is an example of configuration:

```bash
Vagrant.configure("2") do |config|

    config.vm.box = "ubuntu/bionic64" # Type of the machine
        config.vm.hostname = "vagrant"
        config.vm.define "vagrant"
    end

end
```

With this configuration file, we must modify the configuration file adding the shell script (the script that begins with config.vm.provision):

```bash
Vagrant.configure("2") do |config|

    config.vm.box = "ubuntu/bionic64" # Type of the machine
        config.vm.hostname = "vagrant"
        config.vm.define "vagrant"
        config.vm.provision "shell" do |s|
        ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
        s.inline = <<-SHELL
          echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
          echo #{ssh_pub_key} >> /root/.ssh/authorized_keys
        SHELL
    end

end
```

Finally, in order to execute the shell script we just run the command:

```bash
$ vagrant provision
```


## Other commands

List of vagrant commands:

```bash
`box`             manages boxes: installation, removal, etc.
    `add`
    `list`
    `outdated`
    `prune`
    `remove`
    `repackage`
    `update`

`cloud`           manages everything related to Vagrant Cloud
`destroy`         stops and deletes all traces of the vagrant machine
`global-status`   outputs status Vagrant environments for this user
`halt`            stops the vagrant machine
`help`            shows the help for a subcommand
`init`            initializes a new Vagrant environ by creating a Vagrantfile
`login`

`package`         packages a running vagrant environment into a box
`plugin`          manages plugins: install, uninstall, update, etc.
`port`            displays information about guest port mappings
`powershell`      connects to machine via powershell remoting
`provision`       provisions the vagrant machine
`push`            deploys code in this environ. to a configured destination
`rdp`             connects to machine via RDP
`reload`          restarts vagrant machine, loads new file configuration
`resume`          resume a suspended vagrant machine
`snapshot`        manages snapshots: saving, restoring, etc.
`ssh`             connects to machine via SSH
`ssh-config`      outputs OpenSSH valid config to connect to the machine
`status`          outputs status of the vagrant machine
`suspend`         suspends the machine
`up`              starts and provisions the vagrant environment
`upload`          upload to machine via communicator
`validate`        validates the Vagrantfile
`version`         prints current and latest Vagrant version
`winrm`           executes commands on a machine via WinRM
`winrm-config`    outputs WinRM configuration to connect to the machine
```
