# VIRTUALENVWRAPPER

Virtualenvwrapper és un un gestor d'entorn virtuals. La utilitat d'aquesta eina resideix en el fet que administra tots els entorns virtuals en una ruta específica aillada del propi entorn virtual i a més facilita una sèrie d'accessos directes per crear i activar els entorns virtuals.


Un entorn virtual és una eina per crear entorns de python aillats. En un entorn virtual s'hi poden instal·lar paquets de python de forma que si aquest està activat, quan s'executa un script de python aquest utilitzarà les dependències (amb les versions corresponents) de l'entorn virtual en lloc dels paquets del sistema, permitint tenir les dependències dels diferents projectes aillades les unes de les altres. L'entorn virtual també manté aillades les variables d'entorn.


## Prerrequisits

Has de tenir instalat el gestor de paquets de python:

```bash
$ pip install python-pip
```


## Instal·lació

Executa la següent comanda per instal·lar virtualenvwrapper.

```bash
$ apt install virtualenvwrapper
```


## Configuració

Un cop virtualenwrapper està instal·lat, s'han de establir algunes variables d'entorn al teu sistema. Per fer-ho, escriurem aquestes variables d'entorn al final del fitxer *.bashrc*, que s'executa cada cop que s'inicia el terminal:

```bash
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/projects
source /usr/local/bin/virtualenvwrapper.sh
```

Un cop escrit això, és necessari reiniciar el terminal o alternativament:

```bash
$ source ~/.bashrc
```


## Comandes

### Iniciar el projecte

Iniciem el projecte *test* executant al terminal:

```bash
$ mkproject testenv
```

Aquesta comanda crearà un entorn virtual, l'activarà i et portarà a la home d'aquest entorn. Veurem en el nostre terminal:

```bash
(testenv) ~/projects/testenv$
```

El fet de veure a l'esquerra del prompt *(testenv)* significa que l'entorn virtual està activat. També veiem que se'ns ha creat la carpeta *testenv* situat a la carpeta *~/projects/*, ja que és la ruta que s'ha especificat a la variable *PROJECT_HOME*.

A més, veurem que el virtualenvwrapper haurà creat una carpeta *~/.virtualenvs/testenv*, ja que és la ruta que s'ha especificat a la variable *WORKON_HOME*.

Els paquets de python es podran trobar a:

```bash
~/.virtualenvs/testenv/lib/python2.7/site-packages
```

### Activar el projecte

Podem activar l'entorn virtual (i redirigir-nos a l'arrel de l'entorn) executant:

```bash
$ workon testenv
(testenv) ~/projects/testenv$
```

### Eliminar el projecte

Per eliminar el projecte és necessari desactivar l'entorn virtual:

```bash
$ deactivate
```

Eliminar l'entorn virtual:

```bash
rmvirtualenv testenv
```

I per últim, esborrar la carpeta del projecte:

```bash
~/projects/$ rm testenv/ -rf
```
