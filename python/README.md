# PYTHON

## Què és Python?

Python és un llenguatge de programació d'alt nivell i propòsit general molt
utilitzat. Va ser creat per Guido van Rossum l'any 1991. La seva filosofia de
disseny busca llegibilitat en el codi i la seva sintaxi permet als
programadors expressar conceptes en menys línies de codi del que seria
possible en llenguatges com C. També proveeix estructures per permetre
programes més entenedors tant a petita com a gran escala.

Python suporta diversos paradigmes de programació, incloent-hi programació
orientada a objectes (POO), imperativa i també funcional o procedimental.
Presenta un sistema dinàmic i una gestió de la memòria automàtica i té una
gran i exhaustiva biblioteca estàndard.

Com altres llenguatges de programació dinàmics, Python és usat sovint com a un
llenguatge script, però també es fa servir en una àmplia gamma de contextos
no-script. Utilitzant eines desenvolupades per tercers com Py2exe, cx Freeze o
Pyinstaller, el codi Python pot ser reduït a programes executables
independents. Existeixen intèrprets de Python per molts sistemes operatius
diferents.

CPython, la implementació de referència de Python, és programari lliure i de
codi obert i té un model de desenvolupament basat en la comunitat, de
la mateixa manera que la major part de les altres implementacions.
És controlat per l'organització sense ànim de lucre «Python Software
Foundation», creada el 6 març 2001. La missió de la fundació és fomentar el
desenvolupament de la comunitat Python. És responsable de diversos processos
dins de la comunitat, com el desenvolupament de Python, l'administració dels
drets intel·lectuals i d'obtenir fons. 


# Classes, objectes i instàncies

Python és un llenguatge orientat a objectes i per tant, la manera com
s'estructura el codi és amb la definició d'objectes i establint mètodes i
relacions entre ells.

La forma en que es defineixen els objectes és a través de les classes.

Les classes es defineixen amb la paraula reservada *class* seguit del nom
de la classe amb la primera lletra majúscula, i seguit d'un parèntesi.

```python
class Person():
    name = None
```

Aquestes classes són com una espècie de motlla que permetrà crear instàncies
concretes. Aquestes instàncies no són més que el resultat d'aplicar la
construcció d'una classe i obtenir-ne una figura resultant. En aquest
exemple, l'objecte Person es podrà instanciar donant pas a una persona
concreta amb un nom definit.

Per instanciar una classe només fa falta escriure el nom de la classe seguit
d'un parèntesi:

```python
person = Person()
```

## Herència de classes

TODO


## Atributs i mètodes

De forma similar a molts llenguatges de programació, a Python existeixen
les variables (anomenades atributs) i les funcions (anomenats mètodes).

### Atributs

TODO

### Mètodes

Els mètodes es defineixen dins de les classes i ténen la següent forma:

```python
def sum_of_numbers(a, b):
    return a + b
```

Com veiem, es defineixen amb la paraula reservada *def*, seguit del
nom del mètode (en el nostre cas, suma), i seguit d'un parèntesi que
inclou els paràmetres que emprarem dins del mètode (en el nostre cas,
els paràmentres *a* i *b* que seran números). Per últim, dins del
mètode pot haver la clàusula *return*, la qual serà el valor que
retornarà la funció un cop executades les seves instruccions.

La funció es crida amb el nom de la funció i entre parèntesi els
paràmetres que haurà d'emprar:

```python
sum_of_numbers(1, 2)
>>> 3
```

Per veure exemples d'utilització hem de veure els 3 tipus de mètodes
existents a python. Veiem-ho amb una classe exemple anomenada Cotxe:

```python
class Car():

    name = None
    wheels = 4

    def print_car_name(self, name):
        print('This car is a {}'.format(name))

    @classmethod
    def number_of_wheels(cls):
        print('All cars have {} weels'.format(cls.wheels))

    @staticmethod
    def next_itv():
        print(
            'Next itv will be in {}'.format(date.today() + timedelta(years=4))
        )
```

Mètode de classe: És un mètode que utilitzarà atributs de la classe
però no de la instància. En el nostre exemple, una característica
comuna de totes les instàncies de cotxes és que tindran 4 rodes. Tenint
en compte que l'atribut *wheels* és un atribut de classe, però en canvi
l'atribut *name* és un atribut d'instància, potencialment només es podrà
fer servir el primer dels atributs.

Els mètodes de classe porten sempre el decorador *classmethod* incorporat,
i el primer dels paràmetres és *cls* que fa referència a la classe a la que
pertany. Aquests mètodes es poden cridar des de la classe però també des de
la instància, i el paràmetre *cls* vindrà implícit amb la crida, pel que
NO s'haurà d'éspecificar en el moment de cridar la funció.

En l'exemple el mètode de classe és *number_of_wheels*:

```python
Car.number_of_wheels()
>>> 'All cars have 4 wheels'
```

Mètode d'instancia: Aquests mètodes només poden ser cridats des d'una
instància, i similarment als mètodes de classe, tenen com a primer
paràmetre el paràmetre *self* que fa referència a la instància.

En l'exemple el mètode d'instància és *print_car_name*:

```python
car = Car()
car.print_car_name('seat')
>>> 'This car is a seat'
```

Mètode estàtic: Aquests mètodes són completament agnòstics a la classe o a la
instància i simplement s'executaran a partir dels paràmetres de la crida.
Aquest mètodes porten el decorador *staticmethod* definits a la funció.

En l'exemple el mètode estàtic és *next_itv*, que com es pot veure només
depèn del dia d'avui, que no té res a veure amb la classe o la instància.

```python
# Si avui és dia 01-01-2000
Car.next_itv()
>>> 'Next itv will be in 01-01-2004'
```

## Mètodes d'inicialització

Les classes a Python tenen el que s'anomena el mètodes d'inicialització,
que són mètode que es criden automàticament quan s'instancia una classe.

Per una banda hi ha el mètode *__init__* el qual es crida quan s'instancia
la classe i que típicament estableix atributs a la instància a través
dels paràmetres amb que s'ha cridat.

Per altra banda hi ha el mètode *__str__* que retorna una cadena i que
serà el que mostrarà per pantalla quan la instància es cridi a una funció
print.

Veiem un exemple:

```python
class Person():

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname

    def __str__(self):
        return (
            'This person is {} {}'.format(self.name, self.surname)
        )

joanmarc = Person('Joan Marc', 'Soler')
print(joanmarc)
```

Output

```bash
This person is Joan Marc Soler
```

Another trick with init:

```python
if __name__ == '__main__':
    joanmarc = Person('Joan Marc', 'Soler')
    print(joanmarc)
```

This function will be execute if you call the the class directy, so:
 ```bash
 $ python person.py
 This person is Joan Marc Soler
 ```

## Functions

### zip()

The zip() function returns the iterator of tuples based on an iterable object.

Example:

```python
numeric_list = [1, 2, 3]
string_list = ['one', 'two', 'three']

output = zip(string_list, numeric_list)
print(output)
```

```bash
[('one', 1), ('two', 2), ('three', 3)]
```
