'''
Nested list comprehension
'''

class Sale():

    def __init__(self, company, lines):
        self.company = company
        self.lines = lines


class Line():

    def __init__(self, amount):
        self.amount = amount


sales = [
    Sale('A', [Line(10), Line(20)]),
    Sale('B', [Line(30), Line(40)])
]

output = [
    {
        'amount': line.amount,
        'sale': sale.company,
    }
    for sale in sales
    for line in sale.lines
]

print(output)
