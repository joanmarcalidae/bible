# DOCKER

Docker és una plataforma open-source que permet desenvolupar i executar aplicacions. Amb aquesta eina es pot separar les aplicacions de la teva estructura i permet fer un desplegament ràpid (https://docs.docker.com/).

Es pot instal·lar executant:

```bash
sudo apt-get install docker
```


## Docker Compose

Compose és una eina que permet definir i executar múltiples aplicacions de Docker (https://docs.docker.com/compose/).

Es pot instal·lar executant:

```bash
sudo apt-get install docker-compose
```


## Afegir docker al grup de sudoers

Per tal de donar a l'usuari docker privilegis d'administrador i no haver d'estar realitzant sempre les comandes de docker amb sudo, és una bona idea afegir-lo al grup de sudouers. Per fer-ho:

Crea el grup *docker*:

```bash
sudo groupadd docker
```

Afegeix el teu usuari a aquest grup:

```bash
sudo usermod -aG docker $USER
```

Força a aplicar els canvis en el grup:

```bash
newgrp docker
```

Finalment ja pots verificar que pots executar comandes sense sudo:

```bash
docker run hello-world
```


### Introducció

Es pot definir el funcionament del Compose bàsicament en tres passos:

1. Definició de l'entorn de l'aplicació a partir d'un Dockerfile
2. Definició dels serveis que es volen aixecar en l'aplicació en l'arxiu *docker-compose.yml*
3. Aixecament de la infraestructura a partir de *docker-compose up*

Veiem un exemple de tot això amb una infraestructura d'una aplicació basada en un servidor web python (django) y una base de dades relacional (postgres).

```yaml
version: '1'

  db:
    image: postgres
    ports:
      - "54320:5432"
    environment:
      - POSTGRES_USER=docker
      - POSTGRES_PASSWORD=docker
      - POSTGRES_DB=docker

  django:
    build:
      context: .
      args:
        UID: ${_UID:-1000}
        GID: ${_GID:-1000}
      dockerfile: Dockerfile-django
    depends_on:
      - db
    volumes:
      - ./backend:/app
    environment:
      - PYTHONUNBUFFERED=1
    ports:
      - "8000:8000"
    command: pipenv run python ./manage.py runserver 0.0.0.0:8000
```

Com es pot veure, aquesta aplicació aixeca de dues formes diferents els contenidors *db* i *django*. El primer ho fa a partir d'una imatge predefinida (*postgres*) i el segon ho fa a partir d'un *Dockerfile*. Veiem-ho en el següent apartat.

#### Construcció de serveis

##### Serveis a partir d'un Dockerfile

En l'exemple anterior, el servei de *django* es defineix a partir d'un Dockerfile:

```yaml
django:
    build:
      context: .
      dockerfile: Dockerfile-django
```

Per tant és necessari crear un arxiu anomenat *Dockerfile-django*, que estableixi la configuració de l'entorn de l'aplicació. En aquest cas es farà a partir d'una imatge de python i amb un codi semblant a això:

```bash
FROM python:3.6

ENV APPDIR=/app
WORKDIR $APPDIR

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
    postgresql-client postgresql

ADD . $APPDIR
```

#### Serveis a partir d'una imatge

Per altra banda, el servei de la base de dades *db* es construeix a partir d'una imatge predefinida:

```yaml
  db:
    image: postgres
```

#### Diferències entre servei, contenidor i imatge

És molt habitual confondre's amb la terminologia, ja que a priori és difícil apreciar la diferència entre aquests conceptes.

Mentre que amb *docker* controles contenidors, amb *docker-compose* controles serveis.

Quan s'executa la infraestructura amb el *docker-compose up*, el que en realitat s'està fent és aixecar un N serveis amb N contenidors. Per exemple, si en el nostre *docker-compose.yml* tenim definit el següent:

```yaml
web:
      build:
      context: .
      dockerfile: Dockerfile-web

db:
  image: postgres:latest
```

En aixecar la infraestructura es crearà una imatge de web a partir del Dockerfile-web i es descarragarà la imatge de postgres més recent i les adjuntarà a un contenidor amb el mateix nom. Si la nostra aplicació es diu test, i consultem els processos de docker:

```bash
$ docker ps -a

CONTAINER ID   IMAGE        COMMAND          ...      NAMES
1c1683e871dc   test_web    "nginx -g"       ...      test_web_1
a41360558f96   test_db     "postgres -d"    ...      test_db_1

```

L'avantatge del docker-compose és que ara podem aixecar més contenidors de web, però segueix sent el mateix servei, ja que es basa en la imatge *test_web* ja construïda. Si escalem fins a 5 contenidors de web:

```bash
$ docker-compose scale web=5
Creating and starting 2 ... done
Creating and starting 3 ... done
Creating and starting 4 ... done
Creating and starting 5 ... done
```

Finalment tindrem 6 contenidors, basats en 2 serveis i 2 imatges diferents:

```bash
$ docker ps -a  
CONTAINER ID   IMAGE        COMMAND         ...      NAMES
1bf4c939263f   test_web    "nginx -g"      ...      test_web_3
d3033964a44b   test_web    "nginx -g"      ...      test_web_4
649bbda4d0b0   test_web    "nginx -g"      ...      test_web_5
a265ea406727   test_web    "nginx -g"      ...      test_web_2
1c1683e871dc   test_web    "nginx -g"      ...      test_web_1
a41360558f96   test_db     "postgres -d'   ...      test_db_1

```


### Comandes

### Build

Construeix serveis a partir del *docker-compose.yml* i els deixa aturats. Aquest crearà els serveis a partir d'imatges o a partir dels Dockerfile com s'ha explicat anteriorment.

```bash
$ docker-compose build

docker_db_1
docker_django_1
```


### Start, Restart, Stop, Kill

Un cop els serveis s'han construït, es poden iniciar els contenidors a partir d'aquests serveis:

```bash
$ docker-compose start

Starting db ... Done
Starting django ... Done
```

També es poden resetejar (*restart*), aturar (*stop*) o forçar la seva aturada amb *kill*.


### Up

Fa un *build*, *start* i adjunta un contenidor als serveis. 


### Images

Es poden consultar les imatges generades:

```bash
$ docker-compose images

   Container       Repository      Tag       Image Id      Size
----------------------------------------------------------------
docker_db_1       adepa_db        latest   084ec18124c8   218 MB
docker_django_1   docker_django   latest   fefa08a16f75   898 MB
```


### Containers

Es poden consultar els contenidors creats a partir dels serveis:
```bash
$ docker-compose ps

     Name                  Command            State    Ports
------------------------------------------------------------
docker_db_1       docker-entrypoint.sh psql   Exit 2
docker_django_1   python3                     Exit 0
```


### Run

Un cop els serveis has estat creat, podem fer-los córrer amb *docker-compose run <servei>*. Aquest executarà un contenidor basat en el servei especificat i també els serveis que depenen d'ell. Si per exemple, el servei de *django* necessita una base de dades i depèn del servei *db*. Ens trobarem:

```bash
$ docker-compose run django

Starting test_django ... done
Starting test_db ... done
```

Per defecte, el run executa la comanda especificada en el servei al *docker-compose.yml*. Alternativament podem especificar una comanda. Així doncs, si el contenidor de django per defecte executava l'executava una comanda de python:

```yml
  django:
    build:
      context: .
      dockerfile: Dockerfile-django
    depends_on:
      - db
    command: pipenv run python ./manage.py runserver 0.0.0.0:8000
```

Podem especificar una altra comanda (i.e. bash), tal que:

```bash
$ docker-compose run django bash

Starting test_django_1 ... done
Starting test_db_1 ... done
root@58cde1562471:/app#
```


### Exec

A diferència del run, aquest executa una comanda sobre un contenidor existent en comptes de crear un de nou.


### Scale

Escala el nombre de contenidors basats en un servei concret. Si per exemple, volem escalar a 5 contenidors de *django*, executariem:

```bash
$ docker-compose scale django=5
```


### Down

La comanda down elimina els contenidors i les imatges, pel que seria com esborrar tota la infraestructura creada.
