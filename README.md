# BIBLE

## Introducció

Aquest repositori neix a partir dels apunts que he près des que vaig començar la meva aventura com a backend software developer.

L'objectiu d'aquest projecte no és ser una documentació extensiva, i molt menys, una guia única. L'objectiu és explicar les eines que fem servir (o hem fet servir en algun moment) a Calidae, i ajudar als nous desenvolupadors a familiaritzar-se amb aquestes eines.


## Estructura

En aquest repositori trobaràs carpetes que contenen les diferents eines que ens volen explicar. Cadascuna d'elles conté un fitxer *readme.md* que defineix l'eina i ajuda a entendre tots els conceptes relacionats, i també  pot contenir alguns fitxers necessaris per fer servir l'eina en qüestió. A mode d'exemple, la carpeta *ansible* conté un readme amb l'explicació de l'ansible i tot allò relacionat amb ansible (playbooks, rols, inventaris, etc.) i també carpetes i fitxers que permeten llençar un playbook a mode d'exemple.


## Eines

### Ansible

Configuració i administració d'infraestructura com a codi.

### Bash

Llenguatge de comandes i shell d'Unix.

### Django

Framework d'aplicacions web desenvolupades amb Python.

### Docker

Plataforma open-source que permet desenvolupar i executar aplicacions.

### Git

Software de control de versions de desenvolupament.

### Postgresql

Sistema de gestió de bases de dades relacional orientat a objectes.

### Python

Llenguatge de programació interpretat la filosofia del qual és fer incís en la seva legibilitat.

### SQL

Llenguatge de domini específic desenvolupat per administrar i recuperar informació en sistema de gestió de bases de dades.

### Terraform

Definició d'infraestructura com a codi.

### Tryton

ERP open source desenvolupat en python i xml.

### Vagrant

Creació de màquines virtuals que emulen servidors dins del propi sistema.

### Virtualenvwrapper

Gestor d'entorns virtuals.

### Welcome Pack

Breu explicació de com gestionar les noves incorporacions a Calidae.

## Altres programes

Tot i que aquest repositori no conté informació relacionada amb els programes definits a continuació, és aconsellable que el nou desenvolupador els conegui i es familiaritzi amb ells.

### DBeaver

Gestor de bases de dades amb interfície gràfica.

### Terminator

Terminal de Linux personalitzable que conté multitud de funcionalitats, sent la més important la possibilitat d'executar múltiples consoles en un sol terminal.

### Visual Studio Code

Editor de text que permet multitud de plugins i que és molt útil per desenvolupar software en multitud de llenguatges.


## Autor

Joan Marc Soler
