# Object Relational Mapping (ORM)

If you deal with objects, such the object Person(), it can't be stored in a database. That is because the databasa deal with scalars and you have to store fields like name, address or something else.

The ORM is the tool that try to approximate those objects into scalars in order to allow to store them in a database.


## Associating mapping

Let's go explain the association between objects.

One-to-one: A one-to-one relationship exists when one row in a table may be linked with only one row in another table and vice versa. It is important to note that a one-to-one relationship is not a property of the data, but rather of the relationship itself. A list of mothers and their children may happen to describe mothers with only one child, in which case one row of the mothers table will refer to only one row of the children table and vice versa, but the relationship itself is not one-to-one, because mothers may have more than one child, thus forming a one-to-many relationship.

Example:

```
Country <--> Capital City
```

One-to-many: A one-to-many relationship exists when one row in table A may be linked with many rows in table B, but one row in table B is linked to only one row in table A. It is important to note that a one-to-many relationship is not a property of the data, but rather of the relationship itself. A list of authors and their books may happen to describe books with only one author, in which case one row of the books table will refer to only one row of the authors table, but the relationship itself is not one-to-many, because books may have more than one author, forming a many-to-many relationship.

Example:

```
        /-->
Book ------> Pages
        \-->
```

Many-to-many: In a relational database management system, such relationships are usually implemented by means of an associative table (also known as join table, junction table or cross-reference table), say, AB with two one-to-many relationships A -> AB and B -> AB. In this case the logical primary key for AB is formed from the two foreign keys (i.e. copies of the primary keys of A and B).

Example:

```
      <--\  /-->
Books <--------> Authors
      <--/  \-->
```