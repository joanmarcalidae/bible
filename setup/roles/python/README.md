python3
=========

Checks for python3 and if is not present install's it.

Requirements
------------

Remote host must have an Ubuntu cersion with python3 in apt repositories by default.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      gather_facts: false
      roles:
         - { role: calidae.python3 }

License
-------

MIT
