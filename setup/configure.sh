#!/bin/bash

cat <<EOF > .env
ANSIBLE_INVENTORY=./docker_inventory
ANSIBLE_STDOUT_CALLBACK=debug
ANSIBLE_FORCE_COLOR=True
ANSIBLE_VAULT_PASSWORD_FILE=./vault-keyring.py

EOF