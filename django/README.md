# DJANGO

## API

See *readme.md* in api folder.

## CSS and JS

If it's needed to add CSS or JavaScript to views is it possible to do it with class Media. In the ModelForm view:

```python
class PersonAdminForm(forms.ModelForm):
    class Meta:
        fields = '__all__'

    class Media:
        css = {'all': ('admin/css/person.css',)}
        js = ('admin/js/person.js',)
```

And you can add to /admin/css the *person.css* file:

```css
.field-name {
    background-color: #000000;
}
```

And in /admin/js:

```js
(function($){
  $(document).ready(function() {
    $('.field-name').click(function () {
      alert("You clicked the name");
    });
  });
})(django.jQuery);
```

And that's all.


## Translation

You are not supposed to us verbose name in order to translate your code. The way to procedur is using the *gettext* library from *django.utils.tranlation* .

Let's gona see an example:

```python
from django.db import models
from django.utils.translation import gettext_lazy as _

class MyThing(models.Model):
    kind = models.ForeignKey(
        ThingKind,
        on_delete=models.CASCADE,
        related_name='kinds',
        verbose_name=_('kind'),
    )
```

So verbose name 'kind' want to be translated. But previously, we have to define a list of translations in settings:

```python
LANGUAGES = [
  ('ca', 'Català'),
  ('es', 'Español'),
  ('en', 'English'),
]
```

Now, in order to translate to catalan you can generate the *.po* files with the command:

```bash
python manage.py makemessages -l 'ca'
```

This command will create a folder named *locale* and inside it another folder called *ca*. This ones contain the *.po* file that allow you make translations using poedit or whatever you want to use. Once it is finnished, execute the command:

```bash
python manage.py compilemessages
```

That will generate the *.mo* binary files that allow django to translate.

## Dynamic fields

### HStoreField

There are few options to get dynamic fields in Django framework. Here we will
see a solution based in *HStoreField* class. This solution is based in a postgres database. If we take a look on dejavu project, we will notice that there is a database dictionari that look like this:

```bash
DATABASES = {
    'default': {
        'ENGINE': os.getenv("DB_ENGINE", 'django.db.backends.postgresql'),
        'NAME': os.getenv("DB_NAME", 'docker'),
        'USER': os.getenv("DB_USER", 'docker'),
        'PASSWORD': os.getenv("DB_PASSWD", 'docker'),
        'HOST': os.getenv("DB_HOST", 'localhost' if LOCAL else 'db'),
        'PORT': os.getenv("DB_PORT", '54320' if LOCAL else '5432'),
    }
}
```

So it is important to set *django.db.backends.postgresql* to our database.

Then, we have to prepare our database to create an extension for hstore extension, so a migration is needed.

In order to migrate, we will add a migration, with the following migration
file.

```python
from django.contrib.postgres.operations import HStoreExtension

class Migration(migrations.Migration):

    operations = [
        HStoreExtension(),
    ]
```
So once migration is created (it is like *python manage.py make-migration*
does), we must apply migrations with *python manage.py migrate*.

```bash
python manage.py migrate
```

Once created, let's see an example of a model with a dynamic field. We have the model Dog that has a name and we will create a field named data that will store a key-value dict and will have some interesting facts as can be created, filtered, etc.

Let's see the model:

```python
from django.contrib.postgres.fields import HStoreField
from django.db import models

class Dog(models.Model):
    name = models.CharField(max_length=200)
    data = HStoreField()

    def __str__(self):
        return self.name
```

Now we can create an instance of dog that additionally has a breed type, and we can filter with *contains* lookup:

```python
>>> Dog.objects.create(name='Peke', data={'breed': 'labrador'})
>>> Dog.objects.create(name='Noa', data={'breed': 'collie'})

>>> Dog.objects.filter(data__breed='collie')
<QuerySet [<Dog: Noa>]>
```

Obviously, it can also be filtered with *contained_by* lookup:

```python
>>> Dog.objects.create(name='Peke', data={'breed': 'labrador'})
>>> Dog.objects.create(name='Noa', data={'breed': 'collie', 'owner': 'Joan Marc'})
>>> Dog.objects.create(name='Oddie', data={})

>>> Dog.objects.filter(data__contained_by={'breed': 'collie', 'owner': 'Joan Marc'})
<QuerySet [<Dog: Noa>, <Dog: Oddie>]>

>>> Dog.objects.filter(data__contained_by={'breed': 'collie'})
<QuerySet [<Dog: Oddie>]>
```

There are also more lookups (*has_key*, *has_any_keys*, *has_keys*, *keys*, *values*) that can be check in:

https://docs.djangoproject.com/en/2.2/ref/contrib/postgres/fields/#hstorefield


### JSONField

This option is quite similar to HStoreField but this time we are using the JSONField class. An example of model is the following:

```python
from django.contrib.postgres.fields import JSONField
from django.db import models

class Dog(models.Model):
    name = models.CharField(max_length=200)
    data = JSONField()

    def __str__(self):  # __unicode__ on Python 2
        return self.name
```

An example of creating and filtering a *Dog* object could be:

```python
>>> Dog.objects.create(name='Peke', data={
...     'breed': 'labrador',
...     'owner': {
...         'name': 'Joan Marc',
...         'other_pets': [{
...             'name': 'Noa',
...         }],
...     },
... })
>>> Dog.objects.create(name='Oddie', data={'breed': 'collie'})

>>> Dog.objects.filter(data__breed='collie')
[<Dog: Oddie>]
```
