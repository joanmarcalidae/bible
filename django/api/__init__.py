from django.conf.urls import url

from .dynamic_field_value import DynamicFieldValueView
from .routers import HybridRouter

api_router = HybridRouter()

api_router.add_api_view(
    'dynamic_field_values',
    url(
        r'^dynamic_field_values/$',
        DynamicFieldValueView.as_view(),
        name='dynamic_field_values'
    )
)
