# Django API

A common tool that allow to use an API is django-rest-framework. There are a few files involved when create an API point. In this example, in project/api folder we will create the file *my_model.py*:

```python
from rest_framework.response import Response
from rest_framework.views import APIView
from models import MyModel


class MyModelView(APIView):

    def get(self, request):
        """
        Returns a query of all objects from MyModel
        """
        model_values = MyModel.objects.all()
        return Response(model_values)
```

Remember to declare this class in a *__init__.py* in order to get it recognized by python interpreter:

```python
# file = __init__.py
from django.conf.urls import url
from .my_model import MyModelView
from .routers import HybridRouter

api_router = HybridRouter()

api_router.add_api_view(
    'my_model',
    url(
        r'^my_model/$',
        MyModelView.as_view(),
        name='my_model'
    )
)
```

And add the *routers.py* file and it's done!