from rest_framework.response import Response
from rest_framework.views import APIView

from gestionactivos.models import DynamicFieldValue


class DynamicFieldValueView(APIView):

    def get(self, request):
        """
        Returns a Dynamic Field Value list of distinct values to prevent
        duplicity of same entity and distinct written values.
        """
        dynamic_field_values = DynamicFieldValue.objects.distinct(
            'value').values_list('value', flat=True)
        return Response(dynamic_field_values)
